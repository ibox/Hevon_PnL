/**
 * Created by ibox on 11/29/14.
 */
SyncedCron.options = {
    //Log job run details to console
    log: true,

    //Name of collection to use for synchronisation and logging
    collectionName: 'ibox_history_Hevon_PnL',

    //True to use UTC
    utc: true // TODO : to check

    //TTL in seconds for history records in collection to expire
    //NOTE: Unset to remove expiry but ensure you remove the index from
    //mongo by hand
    //,collectionTTL: 172800
};

function updatePnL(result, pair, delta, state){

    //console.log('updatePnL '+delta);

    var ccy_from = pair.split('/')[0],
        ccy_to = pair.split('/')[1];

    if(result[ccy_from]/* && !result[reverse_pair]*/){

        result[ccy_from][state] += delta; //overview for this ccy

        //detail for this ccy
        if(result[ccy_from][ccy_to]){//on which pair we took profit on which one we lost
            if(result[ccy_from][ccy_to][state]){
                result[ccy_from][ccy_to][state] += delta;//detail for this ccy
            }
            else{
                result[ccy_from][ccy_to][state] = 0;
                result[ccy_from][ccy_to][state] += delta;//detail for this ccy
            }
        }
        else{
            result[ccy_from][ccy_to] = {};
            result[ccy_from][ccy_to][state] = 0;
            result[ccy_from][ccy_to][state] += delta;//detail for this ccy
        }
        //result[pair][state] += delta;
        console.log('1');
    }
    /*else if(!result[pair] && result[reverse_pair]){
        result[reverse_pair][state] += delta;
        console.log('2');
    }*/
    else{//first time we check that pair
        result[ccy_from] = {};
        result[ccy_from][state] = 0;
        result[ccy_from][state] += delta; //overview for this ccy
        result[ccy_from][ccy_to] = {};
        result[ccy_from][ccy_to][state] = 0;
        result[ccy_from][ccy_to][state] += delta;//detail for this ccy
        //result[pair][state] += delta;
        console.log('3');
    }
    //-------------------------
}

SyncedCron.add({
    name: 'Hevon_PnL', //Check what we HAVE TO convert
    schedule: function (parser) {
        // parser is a later.parse object
        return parser.text('every 5 seconds');
    },
    job: function () {
        var currentISODate = new Date().toISOString(),
            transacID = '', conversionsList = [], amount_from = '', rate_displayed = 0,
            amount_converted = 0, delta = 0, pnlResult = {}, pair = '';

        console.log('**************************************************************************');
        console.log('******************* Hevon Profit & Loss : START **************************');
        console.log('**************************************************************************');
        console.log('');
        console.log('Hello worker : Hevon PnL ' + currentISODate);

        //set up date in pnl object
        //pnlResult = {date:currentISODate};

        //TODO
        //parse all transacs tagged 'done' and do the maths
        // then the transacs will be updated to 'closed' with a module added
        // to know if there is Profit or Loss with data
        // OR what if this data would be added in a table Data Analysis?

        var transactionsList = Transaction_user.find({status:'done'});

        console.log(transactionsList.count() + ' transactions found');

        transactionsList.forEach(function (ops){

            //setting up variables
            transacID = ops._id;
            amount_from = ops.amount_from;//parseFloat(ops.amount_from);
            rate_displayed = ops.midmarket_displayed;
            conversionsList = ops.conversions;
            amount_converted = 0;
            pair = ops.ccy_from + '/' + ops.ccy_to;
            //reverse_pair = ops.ccy_to + '/' + ops.ccy_from;

            console.log('Analysing transaction : ' + transacID);
            //console.log('TEST1 : '+JSON.stringify(conversionsList[0],null,4));
            for(var index in conversionsList){
                //console.log('TEST2 : '+JSON.stringify(conversionsList[index],null,4));
                //console.log('Aggregating : ' + conversionsList[index].amount_converted);
                if(conversionsList.hasOwnProperty(index) && conversionsList[index].amount_converted != 0.01){
                    amount_converted += /*parseFloat(*/conversionsList[index].amount_converted; //to know how much we needed
                }
            }

            if(amount_from < amount_converted){ // we lost money because we requested more ccy_from than initially planned
                delta = amount_converted - amount_from;
                //DB update -> Transactions
                Transaction_user.update(
                    {_id:transacID},
                    {
                        $set:{
                            status:'closed',
                            pnl:'loss'
                        }
                    },
                    {upsert:true}
                );
                console.log('Transaction ' + transacID + ' closed');

                updatePnL(pnlResult, pair, delta, 'loss');

                console.log('BAD : We lost money on transaction --> ' + delta + ' to inject in the ccy from (through reserve or external)');
                console.log(amount_from + ' ' + ops.ccy_from + ' given by client and '
                + amount_converted + ' ' + ops.ccy_from + ' necessary.');
            }
            else if(amount_from == amount_converted){ // everything happened fine
                //DB update -> Transactions
                Transaction_user.update(
                    {_id:transacID},
                    {
                        $set:{
                            status:'closed',
                            pnl:'equal'
                        }
                    },
                    {upsert:true}
                );
                console.log('Transaction ' + transacID + ' closed');


                console.log('OK : We converted the exact amount of money on transaction');
                console.log(amount_from + ' ' + ops.ccy_from + ' given by client and '
                + amount_converted + ' ' + ops.ccy_from + ' necessary.');
            }
            else if(amount_from > amount_converted){ // we won money because we converted at better rate(s) than the one asked
                delta = amount_from - amount_converted;
                //DB update -> Transactions
                Transaction_user.update(
                    {_id:transacID},
                    {
                        $set:{
                            status:'closed',
                            pnl:'profit'
                        }
                    },
                    {upsert:true}
                );
                console.log('Transaction ' + transacID + ' closed');

                updatePnL(pnlResult, pair, delta, 'profit');

                console.log('GOOD : We won money on transaction --> Moving ' + delta + ' in ccy reserve');
                console.log(amount_from + ' ' + ops.ccy_from + ' given by client and '
                + amount_converted + ' ' + ops.ccy_from + ' necessary.');
            }
            else{
                //console.log(amount_from + ' ' + ops.ccy_from + ' given by client and '
                //+ amount_converted + ' ' + ops.ccy_from + ' necessary.');
                console.log('############ Use case not planned...');
            }
            console.log('------------------ Next transaction ---------------------');

        });

        pnlResultTemp = _.extend(pnlResult,{date:currentISODate});

        console.log('CHECK : '+JSON.stringify(pnlResultTemp,null,4));

        //we store global daily stats
        PnL.insert(pnlResultTemp,
            function(error,resultId) {
                if (error) {
                    console.log('Hevon PnL error : ' + error.reason);
                }else {
                    console.log('Hevon PnL : insert OK');
                }
            }
        );


    }
});